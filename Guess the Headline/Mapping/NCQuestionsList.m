//
//  NCQuestionsList.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCQuestionsList.h"
#import "NCQuestion.h"

@implementation NCQuestionsList

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // properties defined in header < : > key in JSON Dictionary
    return @{ };
}

+ (NSValueTransformer *)itemsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:NCQuestion.class];
}

@end
