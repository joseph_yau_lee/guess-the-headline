//
//  NCQuestionsList.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Mantle.h"

@interface NCQuestionsList : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *product;
@property (nonatomic, copy) NSNumber *resultSize;
@property (nonatomic, copy) NSNumber *version;
@property (nonatomic, copy) NSArray *items; // An array of NCQuestions

@end
