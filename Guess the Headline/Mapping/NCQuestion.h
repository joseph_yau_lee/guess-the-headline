//
//  NCQuestion.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Mantle.h"

@interface NCQuestion : MTLModel <MTLJSONSerializing>

// properties from api response
@property (nonatomic, copy) NSNumber *correctAnswerIndex;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *standFirst;
@property (nonatomic, copy) NSString *storyUrl;
@property (nonatomic, copy) NSString *section;
@property (nonatomic, copy) NSArray *headlines;

// custom properties
@property (nonatomic, copy) NSString *correctHeadlineAnswer;

@end
