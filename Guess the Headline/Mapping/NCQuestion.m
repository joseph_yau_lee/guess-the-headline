//
//  NCQuestion.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCQuestion.h"

@implementation NCQuestion

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // properties defined in header < : > key in JSON Dictionary
    return @{  };
}

- (NSString *)correctHeadlineAnswer
{
    return [self.headlines objectAtIndex:self.correctAnswerIndex.integerValue];
}

@end
