//
//  NCApiManager.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCApiManager.h"

NSString *const kBaseUrl = @"https://dl.dropboxusercontent.com";

@implementation NCApiManager

+ (NCApiManager *)sharedClient
{
    static NCApiManager *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kBaseUrl]];
    });
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    
    // Set acceptable content types
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    
    return self;
}

- (void)getQuestions:(void(^)(NSURLSessionDataTask *task, NCQuestionsList *responseObject))success
             failure:(void(^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = @"/u/30107414/game.json";
    
    [self GET:url parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          if (success) {
              success(task, [NCApiResponseDescriptor mapQuestionsList:responseObject]);
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          if (failure) {
              failure(task, error);
          }
      }];
}

- (void)getImageWithUrl:(NSString *)imageUrl
            withSuccess:(void(^)(UIImage *imageResponse))getSuccess
                failure:(void(^)(NSError *error))getError
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        getSuccess(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        getError(error);
    }];
    [requestOperation start];
}

@end
