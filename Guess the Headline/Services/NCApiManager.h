//
//  NCApiManager.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

#import "NCApiResponseDescriptor.h"

@interface NCApiManager : AFHTTPSessionManager

extern NSString *const kBaseUrl;

+ (NCApiManager *)sharedClient;

// Get questions
- (void)getQuestions:(void(^)(NSURLSessionDataTask *task, NCQuestionsList *responseObject))success
             failure:(void(^)(NSURLSessionDataTask *task, NSError *error))failure;

// Get Image from url link
- (void)getImageWithUrl:(NSString *)imageUrl
            withSuccess:(void(^)(UIImage *imageResponse))getSuccess
                failure:(void(^)(NSError *error))getError;

@end
