//
//  NCObjectMapperHelper.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCApiResponseDescriptor.h"
#import "Mantle.h"

// Classes to parse
#import "NCQuestionsList.h"

@implementation NCApiResponseDescriptor

+ (NCQuestionsList *)mapQuestionsList:(id)response
{
    return [MTLJSONAdapter modelOfClass:NCQuestionsList.class
                     fromJSONDictionary:response
                                  error:NULL];
}

@end
