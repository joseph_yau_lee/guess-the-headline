//
//  NCObjectMapperHelper.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NCQuestionsList;

@interface NCApiResponseDescriptor : NSObject

+ (NCQuestionsList *)mapQuestionsList:(id)response;

@end
