//
//  NCQuestionCollectionViewCell.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCQuestionsViewController.h"
#import "NCQuestionCollectionViewCell.h"
#import "NCQuestion.h"

// API Client
#import "NCApiManager.h"

#define kButtonEnabledColor [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8]
#define kButtonDisabledColor [UIColor lightGrayColor]

@implementation NCQuestionCollectionViewCell {
    // Delegate
    NCQuestionsViewController *vcDelegate;
    
    // Cell variables
    NSTimer *timer;
    NCQuestion *thisQuestion;
    NSIndexPath *thisIndexPath;
    NSInteger guessesRemaining;
}

- (void)awakeFromNib {
    // Init custom properties
    for (UIButton *button in self.buttonCollection) {
        button.layer.cornerRadius = 5.0f;
        button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        button.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
        button.enabled = YES;
    }
    
    // Round Labels
    for (UILabel *label in self.labelCollection) {
        label.clipsToBounds = YES;
        label.layer.cornerRadius = 5.0f;
    }
    
    // Image
    self.headlineImageContainerView.layer.cornerRadius = 5.0f;
    self.headlineImageContainerView.clipsToBounds = YES;
}

#pragma mark - Cell properties

// Method to set cell properties
- (void)setCellProperties:(id)delegate question:(NCQuestion *)question questionIndex:(NSIndexPath *)indexPath {
    
    // Init cell properties
    vcDelegate = delegate;
    thisQuestion = question;
    thisIndexPath = indexPath;
    guessesRemaining = 2;
    
    [self.questionBodyViewFront setAlpha:1];
    [self.questionBodyViewFront.superview bringSubviewToFront:self.questionBodyViewFront];
    
    [self.questionBodyViewBack setAlpha:0];
    
    // Set answers
    [self.firstAnswerButton setEnabled:YES];
    [self.firstAnswerButton setBackgroundColor:kButtonEnabledColor];
    [self.firstAnswerButton setTitle:[question.headlines objectAtIndex:0] forState:UIControlStateNormal];
    
    [self.secondAnswerButton setEnabled:YES];
    [self.secondAnswerButton setBackgroundColor:kButtonEnabledColor];
    [self.secondAnswerButton setTitle:[question.headlines objectAtIndex:1] forState:UIControlStateNormal];
    
    [self.thirdAnswerButton setBackgroundColor:kButtonEnabledColor];
    [self.thirdAnswerButton setEnabled:YES];
    [self.thirdAnswerButton setTitle:[question.headlines objectAtIndex:2] forState:UIControlStateNormal];
    
    // Question Number
    self.questionNumberLabel.text = [NSString stringWithFormat:@"Q%d", thisIndexPath.row+1];
    
    // Category
    self.categoryLabel.text = [question.section capitalizedString];
    
    // Image
    [self loadHeadlineImage:question.imageUrl];
    
    // Answer Heading
    self.questionAnswerHeadlineLabel.text = @"";
    NSString *answerHeadline = [NSString stringWithFormat:@"%@\n%@", question.correctHeadlineAnswer, question.standFirst];
    NSMutableAttributedString *attrAnswerHeadline = [[NSMutableAttributedString alloc] initWithString:answerHeadline];
    [attrAnswerHeadline addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Optima-Bold" size:14.0] range:NSMakeRange(0, question.correctHeadlineAnswer.length)];
    [attrAnswerHeadline addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Optima-Regular" size:12.0] range:NSMakeRange(answerHeadline.length - question.standFirst.length, question.standFirst.length)];
    self.questionAnswerHeadlineLabel.attributedText = attrAnswerHeadline;
    
}

#pragma mark - Button Events

- (IBAction)firstAnswerPressed:(id)sender {
    if (thisQuestion.correctAnswerIndex.intValue == 0) {
        // Correct Answer
        [self correctAnswer];
    }
    else {
        [self incorrectAnswer:self.firstAnswerButton];
    }
}

- (IBAction)secondAnswerPressed:(id)sender {
    if (thisQuestion.correctAnswerIndex.intValue == 1) {
        // Correct Answer
        [self correctAnswer];
    }
    else {
        [self incorrectAnswer:self.secondAnswerButton];
    }
    
}

- (IBAction)thirdAnswerPressed:(id)sender {
    if (thisQuestion.correctAnswerIndex.intValue == 2) {
        // Correct Answer
        [self correctAnswer];
    }
    else {
        [self incorrectAnswer:self.thirdAnswerButton];
    }
}

- (IBAction)skipPressed:(id)sender {
    [timer invalidate];
    self.timerProgressView.progress = 1.0;
    
    // Show answer
    [self animateShowAnswer];
}

- (IBAction)viewArticlePressed:(id)sender {
    // Present view article
    if ([vcDelegate respondsToSelector:@selector(viewArticleSelected:)]) {
        [vcDelegate viewArticleSelected:thisQuestion.storyUrl];
    }
}
- (IBAction)nextQuestionPressed:(id)sender {
    if ([vcDelegate respondsToSelector:@selector(nextQuestionSelected:)]) {
        [vcDelegate nextQuestionSelected:thisIndexPath];
    }
}

#pragma mark - Game logic

- (void)startTimer
{
    [self.timerProgressView setProgress:0];
    [timer invalidate];
    timer = nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.01f
                                             target:self
                                           selector:@selector(updateTimer)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)updateTimer
{
    // 10 second timer
    if (self.timerProgressView.progress < 1.0) {
        self.timerProgressView.progress += (0.01f / 10.0f);
    }
    else {
        // Show answer
        [self skipPressed:nil];
    }
}

- (NSInteger)getQuestionScore
{
    return (10 - (int)(self.timerProgressView.progress * 10));
}

- (void)correctAnswer {
    // Stop timer
    [timer invalidate];
    
    // Show answer
    [self animateShowAnswer];
}

- (void)incorrectAnswer:(UIButton *)button {
    // Disable button
    [button setEnabled:NO];
    [button setBackgroundColor:kButtonDisabledColor];
    
    // Deduct seconds and points
    self.timerProgressView.progress += 0.2;
    
    guessesRemaining -= 1;
    if (guessesRemaining == 0) {
        // No more guesses remaining - flip card
        [self skipPressed:nil];
    }
}

#pragma mark - Animation

// Show question answer
- (void)animateShowAnswer
{
    // Calculate score
    self.questionPointsLabel.text = [NSString stringWithFormat:@"Round: +%ld", (long)[self getQuestionScore]];
    
    // Update total score
    self.totalPointsLabel.text = [NSString stringWithFormat:@"Total: %ld", [vcDelegate getCurrentScore] + (long)[self getQuestionScore]];

    [UIView animateWithDuration:1 animations:^{
        [self.questionBodyViewFront setAlpha:0];
        [self.questionBodyViewBack setAlpha:1];
    } completion:^(BOOL finished) {
        // Update current score
        if ([vcDelegate respondsToSelector:@selector(increaseCurrentScore:)]) {
            [vcDelegate increaseCurrentScore:[self getQuestionScore]];
        }
    }];
}

#pragma mark - API get image

- (void)loadHeadlineImage:(NSString *)imageUrl
{
    self.headlineImageView.image = nil;
    NCQuestionCollectionViewCell * __weak weakSelf = self;
    [[NCApiManager sharedClient] getImageWithUrl:imageUrl
                                     withSuccess:^(UIImage *imageResponse) {
                                         
                                         // Set headline image
                                         weakSelf.headlineImageView.image = imageResponse;
                                         
                                         // Background images
                                         weakSelf.blurredImageView.image = imageResponse;
                                         
                                         // Start showing timer
                                         [weakSelf startTimer];
                                         
                                     } failure:^(NSError *error) {
                                         
                                     }];
}


@end
