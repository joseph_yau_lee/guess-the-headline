//
//  NCFullArticleViewController.h
//  Headline Guess
//
//  Created by CJ on 11/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCFullArticleViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *articleWebView;
@property (assign) NSString *storyUrl;

@end
