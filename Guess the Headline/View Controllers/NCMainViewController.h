//
//  ViewController.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCMainViewController : UIViewController

// Buttons
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@end

