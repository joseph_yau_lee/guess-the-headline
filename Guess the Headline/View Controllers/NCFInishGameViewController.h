//
//  NCFInishGameViewController.h
//  Guess the Headline
//
//  Created by CJ on 16/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCFinishGameViewController : UIViewController

@property (assign) NSInteger finalScore;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end
