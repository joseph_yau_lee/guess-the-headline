//
//  NCFInishGameViewController.m
//  Guess the Headline
//
//  Created by CJ on 16/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCFinishGameViewController.h"

@interface NCFinishGameViewController ()

@end

@implementation NCFinishGameViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Show final score
    self.scoreLabel.text = [NSString stringWithFormat:@"Total: %d", self.finalScore];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
