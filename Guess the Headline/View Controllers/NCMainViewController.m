//
//  ViewController.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCMainViewController.h"
#import "NCQuestionsViewController.h"

// Libraries
#import "MBProgressHud.h"

// Services
#import "NCApiManager.h"

#import "NCQuestionsList.h"

@interface NCMainViewController () {
    NCQuestionsList *questionsList;
}

@end

@implementation NCMainViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initInterfaceObjects];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"startGameSegue"]) {
        NCQuestionsViewController *questionsVC = segue.destinationViewController;
        questionsVC.questionsList = questionsList;
    }
}

#pragma mark - Interface setup

- (void)initInterfaceObjects {
    
    // Clip / Clear the other pieces whichever outside the rounded corner
    self.startButton.clipsToBounds = YES;
    self.startButton.layer.cornerRadius = self.startButton.bounds.size.height / 2;
}

#pragma mark - Button Events

- (IBAction)startButtonPressed:(id)sender {
    [self getQuestionsList];
}

#pragma mark - API Calls

- (void)getQuestionsList {
    // Api call to get questions list
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setDimBackground:YES];
    hud.labelText = @"Loading Questions";
    
    [[NCApiManager sharedClient] getQuestions:^(NSURLSessionDataTask *task, NCQuestionsList *responseObject) {
        [hud hide:YES];
        if (responseObject && responseObject.items.count > 0) {
            // Questions list is returned and has questions
            questionsList = responseObject;
            [self performSegueWithIdentifier:@"startGameSegue" sender:self];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Problem loading questions."
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
            [alert show];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [hud hide:YES];
        NSLog(@"error: %@", error);
        questionsList = nil;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:error.description
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }];
}

@end
