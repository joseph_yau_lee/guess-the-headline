//
//  NCQuestionCollectionViewCell.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NCQuestion;

@interface NCQuestionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *frontView; // top view before flip page animation
@property (weak, nonatomic) IBOutlet UIImageView *blurredImageView;

@property (weak, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@property (weak, nonatomic) IBOutlet UIView *headlineImageContainerView;
@property (weak, nonatomic) IBOutlet UIProgressView *timerProgressView; // timer
@property (weak, nonatomic) IBOutlet UIImageView *headlineImageView; // headline image

@property (weak, nonatomic) IBOutlet UIView *questionBodyViewFront; // container view for body ui elements
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelCollection;


@property (weak, nonatomic) IBOutlet UIButton *firstAnswerButton;
@property (weak, nonatomic) IBOutlet UIButton *secondAnswerButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdAnswerButton;
@property (weak, nonatomic) IBOutlet UIButton *giveUpButton;

@property (weak, nonatomic) IBOutlet UIView *questionBodyViewBack; // View when question answered
@property (weak, nonatomic) IBOutlet UILabel *questionAnswerHeadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPointsLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewArticleButton;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;


// Methods
- (void)setCellProperties:(id)delegate question:(NCQuestion *)question questionIndex:(NSIndexPath *)indexPath;

@end
