//
//  NCQuestionsViewController.m
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCQuestionsViewController.h"
#import "NCFullArticleViewController.h"
#import "NCFinishGameViewController.h"

// Api client service
#import "NCApiManager.h"

#import "NCQuestionsList.h"
#import "NCQuestion.h"
#import "NCQuestionCollectionViewCell.h"

@interface NCQuestionsViewController () {
    NSInteger currentScore;
}

@end

@implementation NCQuestionsViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Init game variable
    currentScore = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollection View Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.questionsList.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    NCQuestionCollectionViewCell *cell = [self.questionsCollectionView dequeueReusableCellWithReuseIdentifier:@"questionCell" forIndexPath:indexPath];
    [cell setCellProperties:self question:[self.questionsList.items objectAtIndex:indexPath.row] questionIndex:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Adjust cell size for orientation
    return self.questionsCollectionView.bounds.size;
}

#pragma mark - Button Events

- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - View Flow

- (NSInteger)getCurrentScore
{
    return currentScore;
}

- (void)increaseCurrentScore:(NSInteger)score
{
    currentScore += score;
}

- (void)nextQuestionSelected:(NSIndexPath *)indexPath
{
    // Scroll to next question
    if (indexPath.row + 1 < self.questionsList.items.count) {
        [self.questionsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:indexPath.row+1 inSection:indexPath.section]
                                             atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                                     animated:YES];
    }
    else {
        // questions finished
        [self questionFinished];
    }
}

- (void)viewArticleSelected:(NSString *)urlPath
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NCFullArticleViewController *fullArticleVC = [sb instantiateViewControllerWithIdentifier:@"FullArticleViewController"];
    fullArticleVC.storyUrl = urlPath;
    [self showViewController:fullArticleVC sender:self];
}

- (void)questionFinished
{
    // Show end game
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NCFinishGameViewController *finalGameVC = [sb instantiateViewControllerWithIdentifier:@"FinishGameViewController"];
    finalGameVC.finalScore = currentScore;
    [self showViewController:finalGameVC sender:self];
}

@end
