//
//  NCFullArticleViewController.m
//  Headline Guess
//
//  Created by CJ on 11/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import "NCFullArticleViewController.h"

#import "MBProgressHud.h"

@interface NCFullArticleViewController ()

@end

@implementation NCFullArticleViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Load web article
    [self initWebViewArticle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initWebViewArticle
{
    NSURL *url = [NSURL URLWithString:self.storyUrl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.articleWebView loadRequest:requestObj];
}

#pragma mark - Button Events

- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Webview Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}

@end
