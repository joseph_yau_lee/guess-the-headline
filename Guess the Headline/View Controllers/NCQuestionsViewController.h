//
//  NCQuestionsViewController.h
//  Guess the Headline
//
//  Created by CJ on 10/2/15.
//  Copyright (c) 2015 Joe Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NCQuestionsList;

@interface NCQuestionsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

// Methods for controlling view flow
// Used when uicollectionviewcell passes through a next question command
- (NSInteger)getCurrentScore;
- (void)increaseCurrentScore:(NSInteger)score;
- (void)nextQuestionSelected:(NSIndexPath *)indexPath;
- (void)viewArticleSelected:(NSString *)urlPath;

@property (assign) NCQuestionsList *questionsList;

// UI Objects
@property (weak, nonatomic) IBOutlet UICollectionView *questionsCollectionView;

@end
